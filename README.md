
**Procédure de lancement du projet**

​

Suivre la procédure suivante pour lancer le projet :

1. Migrer la base de donnée avec `python manage.py makemigrations` puis `python manage.pymigrate`.

2. Lancer le script `python launch_project.py` afin de créer une simple base de données avec deux utilisateurs : 

    - un superuser "admin" (pwd:'admin12345')

    - un simpleuser "user" (pwd:'user12345')   

Note: Pour remettre à zéro la base de donnée, ont peut utilser la commande `python launch_project.py delete`.

3. Lancer le serveur à  l'aide de `python manage.py runserver`

4. Se connecter avec un des deux comptes depuis l'url "/connect"

5. Si l'authentification s'est bien déroulée on entre alors dans la vue /home qui nous donne accès à toutes les réservations de l'utilisateur.

6. Dès lors on peut créer des réservation en cliquant sur *ajouter une réservation*.

7. L'utilisateur 'admin' peut quand à lui visualiser toutes les réservations, les supprimer et les modifier. Il peut aussi creer et supprimer de nouvelles ressources.

​

**TODO : Reste à faire**

​

- Ajouter les permissions

- Problèmes avec l'internationaliation (implémenté mais ne fonctionne pas)

- Problème lors de la validation d'un formulaire vide

- Conserver historique des entrées utilisateur dans formulaire

​


