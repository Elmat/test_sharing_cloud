Suivre la procédure suivante pour lancer le projet : 
-> migrer la base de donnée avec 'python manage.py makemigrations' puis 'python manage.pymigrate'
-> lancer le script 'python launch_project.py' afin de créer une simple base de données avec deux utilisateurs : 
	- un superuser "admin" (pwd:'admin12345')
	- un simpleuser "user" (pwd:'user12345')
Pour remettre à zéro la base de donnée, ont peut utilser la commande 'python launch_project.py delete'.
-> lancer le serveur à  l'aide de 'python manage.py runserver'
-> se connecter avec un des deux comptes depuis l'url "/connect"
-> si l'authentification s'est bien déroulée on entre alors dans la vue /home qui nous donne accès à toutes les réservations de l'utilisateur.
-> Dès lors on peut créer des réservation en cliquant sur 'ajouter une réservation'.
-> L'utilisateur 'admin' peut quand à lui visualiser toutes les réservations, les supprimer et les modifier. Il peut aussi creer et supprimer de nouvelles ressources.

