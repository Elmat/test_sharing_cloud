import pytz
from django.utils import timezone
from django.utils.translation import activate

# make sure you add `TimezoneMiddleware` appropriately in settings.py
class TimezoneMiddleware(object):
    """
    Middleware to properly handle the users timezone
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # make sure they are authenticated so we know we have their tz info.
        user = request.user
        if user.is_authenticated and hasattr(user, 'profil'):
            # we are getting the users timezo   ne that in this case is stored in 
            # a user's profile
            tz_str = user.profil.timezone
            timezone.activate(pytz.timezone(tz_str))
            # print("Langue = {}".format(request.user.profil.language))
            # activate(request.user.profil.language)
            #print("Langue = {}".format(request.user.profil.language))
            #activate(request.user.profil.language)
            

        # otherwise deactivate and the default time zone will be used anyway
        else:
            timezone.deactivate()

        response = self.get_response(request)
        return response