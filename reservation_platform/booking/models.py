from django.db import models
from django.utils import timezone
from datetime import datetime
import pytz
from connect.models import Profil

# Register your models here.
class Ressource(models.Model):
    RESRC_TYPES = (
        ("L", "Loft"),
        ("S", "Studios"),
        ("H", "House")
    )
    name = models.CharField(max_length=50)
    res_type = models.CharField(max_length=10, choices=RESRC_TYPES)
    location = models.CharField(max_length=50)
    capacity = models.IntegerField(choices=zip(range(1,10), range(1,10)))
    comments = models.TextField(null=True)
    
    def check_availabilities(self, start_date, end_date):
        reservations = self.related_reservations.all()
        for res in reservations:
            if ((res.start_date <= end_date) and (res.end_date  >= start_date)):
                return False
        return True
        
    def add_booking(self, start_date, end_date):
        if isinstance(start_date, datetime) and isinstance(end_date, datetime):
            if self.check_availabilities(start_date, end_date):
                return True
        return False

    def __str__(self):
        return self.name


class Reservation(models.Model):
    RES_STATUS = (
        ("Passed", "#222"),
        ("Current", "darkgreen"),
        ("Incoming", "lightgreen")
    )
    title = models.CharField(max_length=50)
    start_date = models.DateTimeField(default=timezone.now, verbose_name="Start Date")
    end_date = models.DateTimeField(default=timezone.now, verbose_name="End Date")
    author = models.ForeignKey(Profil, null=True, blank=True, on_delete=models.CASCADE, related_name="profil")
    ressource = models.ForeignKey(Ressource, null=True, on_delete=models.CASCADE, related_name="related_reservations")
    status = models.CharField(max_length=10, choices=RES_STATUS)

    def set_title(self, new_title):
        self.title = new_title

    def status_update(self):
        '''Check status of one reservation'''
        today = datetime.now(timezone.get_current_timezone())
        if self.end_date < today:
            self.status = Reservation.RES_STATUS[0][0]
        elif self.start_date < today and self.end_date > today:
            self.status = Reservation.RES_STATUS[1][0]
        else:
            self.status = Reservation.RES_STATUS[2][0]
        self.save()

    def __str__(self):
        return "RESERVATION {}\n ({}, {}) by {}".format(self.title, self.start_date, self.end_date, self.author)


