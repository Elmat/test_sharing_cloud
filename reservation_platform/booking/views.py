from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from datetime import datetime
from .forms import ReservationForm, RessourceForm
from .models import Reservation, Ressource
from django.contrib import messages 
from django.utils import timezone
import pytz
import sys
import connect
from connect.models import Profil
from django.utils.translation import ugettext as _
from django.utils.translation import get_language, activate

# these two class below just check user authenticated
def check_auth(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/connect')

def check_admin_auth(request):
    if not request.user.is_staff:
        return HttpResponseRedirect('/connect')

def home(request):
    '''View to render /home.html template'''
    check_auth(request)
    user_profil = request.user.profil
    today = timezone.localtime()
    zone = timezone.get_current_timezone()
    
    # TOFIX : here activate language do not works ! 
    activate(request.user.profil.language)
    user_language = get_language()

    # translated paragraphs
    welcome = _("Bienvenue")
    infos = _("Voici l'ensemble de vos réservations sur notre plateforme. Vous \
            pouvez les modifier avec le bouton 'modify' ou encore les supprimer \
            avec le bouton 'X'.")

    # get reservation list to display depending on authenticated user
    reservation_list = Reservation.objects.filter(author=user_profil) if not request.user.is_staff else Reservation.objects.all()
    reservation_list = list(reservation_list.order_by("-start_date")) # sort by dates
    ressource_list = list(Ressource.objects.all())

    # update status each time to check if reservation is "passed", 'current' or 'incoming'
    for res in reservation_list:
        res.status_update()
    return render(request, "home.html", locals())


# id est à 1 que si 
def booking_form(request, id=-1):
    '''View to access booking form'''
    check_auth(request)
    error_message = ""
    form = ReservationForm(request.POST or None)
    ressources = Ressource.objects.all() # get all ressources
    
    # if form is valid then we can create a new Reservation instance
    if form.is_valid(): 
        title = form.cleaned_data['title']
        ressource = form.cleaned_data['ressource']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']

        reservation = Reservation() if id < 0 else Reservation.objects.get(id=id)
        if reservation:
            reservation.title = title
            reservation.ressource = ressource       
            reservation.start_date = start_date
            reservation.end_date = end_date
            reservation.author = request.user.profil
            reservation.status_update()
            reservation.save()
            return HttpResponseRedirect('/home')
    if form.errors:
        error_message = "Entrées du Formulaire non valides"
    return render(request, 'booking_form.html', locals())    

def delete_booking(request, id):
    '''Simple function to delete one reservation'''
    check_auth(request)
    reservation = Reservation.objects.get(id=id)
    
    # delete only if reservation exist
    if reservation:
        reservation.delete()
    return HttpResponseRedirect('/home')


def ressource_form(request):
    # ressource form is available onlu for admin users
    check_admin_auth(request)
    if request.user.has_perm("booking.create_ressource"):
        time =  datetime.now()
        form = RessourceForm(request.POST or None)
        ressources = Ressource.objects.all() # get all ressources
        ressource_types = Ressource.RESRC_TYPES

        # if form is valid then we can create a new Ressource instance
        if form.is_valid(): 
            name = form.cleaned_data['name']
            res_type = form.cleaned_data['res_type']
            location = form.cleaned_data['location']
            capacity = form.cleaned_data['capacity']

            ressource = Ressource()
            ressource.name = name
            ressource.res_type = res_type
            ressource.location = location
            ressource.capacity = capacity
            ressource.save()

            return HttpResponseRedirect('/home')
         
        error_message = form.errors
    return render(request, 'ressource_form.html', locals())    

def delete_ressource(request, id):
    check_admin_auth(request)
    # check if user has permission to delete a ressource
    if request.user.has_perm("booking.delete_ressource"):
        ressource = Ressource.objects.get(id=id)
        if ressource:
            ressource.delete()
        
    return HttpResponseRedirect('/home')
    


