from django.contrib import admin
from .models import Reservation, Ressource



# permet un meilleur affichage et filtrage des modèles dans la vue admin
class RessourceAdmin(admin.ModelAdmin):
   list_display   = ('name', 'res_type', 'location', 'capacity')
   list_filter    = ('res_type','location', 'capacity')
   #date_hierarchy = 'name'
   ordering       = ('name', 'res_type', 'location', 'capacity')
   search_fields  = ('name', 'capacity')
   
   # Colonnes personnalisées 
   def comments_overview(self, ressource):
       """ 
       Retourne les 40 premiers caractères du contenu de l'article. S'il
       y a plus de 40 caractères, il faut rajouter des points de suspension.
       """
       text = ressource.comments[0:40]
       return '%s…' % text
       
    
    #comments_overview.short_description = 'Aperçu du contenu'

   # Configuration du formulaire d'édition
   fieldsets = (
       # Fieldset 1 : meta-info (titre, auteur…)
       ('Général', {
           'classes': ['collapse', ],
           'fields': ('name', 'res_type', 'location', 'capacity')
           }),
        # Fieldset 2 : contenu de l'article
        ('Comments', {
            'description': 'Give comments about this place',
            'fields': ('comments',)
        }),
    )


# we specify django to register these model in admin mode and what kind of display we would like for admin 
admin.site.register(Ressource, RessourceAdmin)

class ReservationAdmin(admin.ModelAdmin):
   list_display   = ('title', 'start_date', 'end_date', 'author')
   list_filter    = ('start_date','end_date')
   #date_hierarchy = 'name'
   ordering       = ('title', 'start_date', 'end_date')
   search_fields  = ('title', 'start_date', 'end_date')

admin.site.register(Reservation, ReservationAdmin)