# write forms here
from django import forms
from .models import Reservation, Ressource
from django.utils import timezone
import datetime


class ReservationForm(forms.ModelForm):
    ''' Reservation form used in /booking'''
    class Meta:
        model=Reservation
        fields=('title', 'start_date', 'end_date', 'ressource')

    def clean(self):
        cleaned_data = super(ReservationForm, self).clean()
        start = cleaned_data['start_date']
        end = cleaned_data['end_date']
        ressource = cleaned_data['ressource']

        # check if there is no anachronism betwen start and end dates
        if start and end: # si les fields sont cleans
            if start >= end:
                raise forms.ValidationError("Unvalid dates !")
        # check if ressource is not booked yet by another user
        if ressource:
            if ressource.add_booking(start, end) == False:
                raise forms.ValidationError("Dates are already booked!")      
        else:
            raise forms.ValidationError("Unvalid Ressource !")
        return cleaned_data



class RessourceForm(forms.ModelForm):
    ''' Ressource form used in /ressource (only available for admin)'''
    class Meta:
        model=Ressource
        fields = ('name', 'res_type', 'location', 'capacity')

