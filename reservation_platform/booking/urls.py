from django.urls import path
from . import views
from django.conf.urls.i18n import i18n_patterns

urlpatterns = [
    path('home/', views.home),
    path('booking/', views.booking_form),
    path('booking/modify/<int:id>', views.booking_form),
    path('booking/delete/<int:id>', views.delete_booking),
    path('ressource/', views.ressource_form),
    path('ressource/delete/<int:id>', views.delete_ressource)
]



