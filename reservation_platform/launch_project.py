# systeme modules
import sys
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "reservation_platform.settings")

# django modules
import django
django.setup()

from django.core.management import call_command
from django.core.wsgi import get_wsgi_application
from connect.models import Profil
from django.contrib.auth.models import User


def check_args(args):
    """Check if args is only 'delete' keyword"""
    if len(args)== 2 and args[1] == "delete":
        return True
    return False
    
def without_keys(d,keys):
    """Return a dictionnay without keys in 'keys'"""
    return {x:d[x] for x in d if x not in keys}


def create_users_in_database(super_user, simple_users_infos):
    if not super_user:
        super_user = User.objects.create_superuser(**without_keys(admin_infos, ["profil"]))
        super_user.save()
        Profil.objects.create(user=super_user, **admin_infos['profil'])
        
    for u in simple_users_infos:
        if not User.objects.filter(username=u["username"]):
            print("Creating user {}".format(u['username']))
            user = User(**without_keys(u, ["profil"]))
            user.save()
            Profil.objects.create(user=user, **u['profil'])
    
def clean_users_in_database(delete_mode=False):
    simple_users_list = User.objects.exclude(username=admin_infos['username'])

    if delete_mode:
        for u in simple_users_list:
            u.delete()
        if super_user:
            super_user.delete()


# ---- USER DATAS ---
# You can add a new admin or simple user here
admin_infos = {'username' : "admin",
            'email' : "admin@example.com",
            'password' : "admin12345",
            'profil' : {
                'timezone': "Australia/Melbourne",
                'language': "fr"
            }}
users_infos = {'username' : "user",
            'email': "user@example.com", 
            'password': "user12345",
            'profil' : {
                'timezone':"Europe/Berlin",
                'language':"en"
            }}

application = get_wsgi_application()

# check launch object input arguments
delete_mode = check_args(sys.argv) 

# list all users and the superuser ...
super_user = User.objects.filter(username=admin_infos['username'])
simple_users_infos = [users_infos]
# ... then creates users if they still not exist
create_users_in_database(super_user, simple_users_infos)

# delete users if delete args is given
clean_users_in_database(delete_mode)

# # migrate all new datas into database
# call_command('makemigrations')
# call_command('migrate')