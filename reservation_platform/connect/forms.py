from django import forms

class ConnectionForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(label="password", widget=forms.PasswordInput)