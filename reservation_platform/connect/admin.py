from django.contrib import admin
from django.contrib.auth.models import User
from .models import Profil
# Register your models here.

class ProfilAdmin(admin.ModelAdmin):     
    list_display = ('user', 'timezone', 'language')

admin.site.register(Profil, ProfilAdmin)