from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
import pytz

TIME_ZONE = (
    ('Europe/Berlin', 'Europe/Berlin'), 
    ('Europe/London', 'Europe/London'),
    ('Europe/Paris', 'Europe/Paris'),
    ('Australia/Melbourne', 'Australia/Melbourne')
)

LANGUAGE = (
    ('fr', 'Francais'),
    ('en', 'English')
)

class Profil(models.Model):
    ''' This class extend "User" native classs, and add timezone and language for each user'''
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True, related_name="profil")  # La liaison OneToOne vers le modèle User
    timezone = models.CharField(max_length=20, choices=TIME_ZONE, null=True, blank=True)
    language = models.CharField(max_length=10, choices=LANGUAGE, null=True, blank=True)

    def __str__(self):
        return "{}".format(self.user.username)

