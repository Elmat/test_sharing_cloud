from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from .forms import ConnectionForm
from booking.models import Reservation, Ressource
from booking.views import booking_form
from django.http import HttpResponse, HttpResponseRedirect, Http404



def connexion(request, error_message = ""):
    '''A simple authentification view to access login form'''
    if request.user.is_authenticated:
        logout(request)
        return render(request, "login.html", locals())

    if request.method == "POST":
        form = ConnectionForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            # check if informations correspond to one registered user
            user = authenticate(username = username, password=password)
            if user: 
                login(request, user)
                error = False
                return HttpResponseRedirect("/home")
            else:
                error = True
    else:
        form = ConnectionForm()
    return render(request, "login.html", locals())
